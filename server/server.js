const express       = require('express')
const path          = require('path')

var app = module.exports = express()

app.use(express.static(path.join(__dirname, '/../public')))
app.use(express.static(path.join(__dirname, '/../node_modules')))

app.use(function(req, res, next) {
  next({status: 404, message: `Not found:  ${req.path}`});
});

app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  console.log('error', err);
  res.end(err.message);
});


