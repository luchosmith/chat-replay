import React from 'react'

const Message = ({user, message}) => {
  return(
    <div className="chat-message">
      <span className="display-name">{user.display_name}</span>
      <span className="message-text">{message.text}</span>
      {message.edited && <span className="message-edited">(edited)</span>}
    </div>
  )
}

export default Message