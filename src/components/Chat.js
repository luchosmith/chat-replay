import React from 'react'
import PropTypes from 'prop-types'
import Message from './Message'

class Chat extends React.Component {
  constructor(props) {
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleSubmit(event) {
    event.preventDefault();
    let url = event.target.transcriptUrl.value
    this.props.onFetchTranscript(url)
  }

  render() {
    const {
      loading,
      messages,
      error
    } = this.props

    return (
      <div className="chat-wrapper">
        <h1 className="header">Chat Replay</h1>
        <div className="transcript-form">
          <form onSubmit={this.handleSubmit}>
            <input type="text" placeholder="url" name="transcriptUrl" />
            <button type="submit">Fetch Transcript</button>
          </form>
        </div>
        { error && <h3 className="header">Error Loading Transcript</h3>}
        { loading && <h3 className="header">loading...</h3>}
        <div className="messages-wrapper">
        {messages.map((message) => {
          return (
            <Message key={message.message.id} {...message} />
          )
        })}
        </div>
      </div>
    )
  }
}

Chat.propTypes = {
  loading: PropTypes.bool.isRequired,
  messages: PropTypes.array.isRequired,
  error: PropTypes.string,
  onFetchTranscript: PropTypes.func.isRequired
}

export default Chat