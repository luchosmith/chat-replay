import uuidv1 from 'uuid/v1'
import {
  FETCH_TRANSCRIPT_LOADING,
  FETCH_TRANSCRIPT_SUCCESS,
  FETCH_TRANSCRIPT_ERROR,
  USER_JOINED_CHAT,
  USER_POSTED_MESSAGE,
  USER_EDITED_MESSAGE,
  USER_DELETED_MESSAGE,
  USER_CHANGED_NAME,
  USER_LEFT_CHAT,
  END,
  EVENT_PROCESS_ERROR } from '../actionTypes'

const getSystemMessage = (text) => {
  return {
    type: 'system',
    user: { 
      id: 0,
      user_name: 'system',
      display_name: 'system'
    },
    message: { 
      id: uuidv1(), 
      text
    } 
  }
} 

export default function mainReducer(state = initialState, action) {

  switch(action.type) {

    case FETCH_TRANSCRIPT_LOADING: {
      return {
        ...state,
        loading: true
      }
    }

    case FETCH_TRANSCRIPT_SUCCESS: {
      return {
        ...state,
        loading: false
      }
    }

    case USER_JOINED_CHAT: {
      let users = state.users.slice()
      users.push(action.payload.user)
      let messages = state.messages.slice()
      messages.push(getSystemMessage(`${action.payload.user.display_name} joined the chat`))

      return {
        ...state,
        users,
        messages
      }
    }

    case USER_POSTED_MESSAGE: {
      
      let users = state.users.slice()
      let user = action.payload.user
      let existingUser = users.find(u => u.id === user.id)
      if (!existingUser ) {
        users.push(user)
      }

      let messages = state.messages.slice()
      messages.push(action.payload)

      return {
        ...state,
        messages,
        users
      }
    }

    case USER_CHANGED_NAME: {
      let users = state.users.slice()
      let user = users.find((u) => {
        return u.id === action.payload.user.id
      })

      let messages = state.messages.slice()
      messages.push(getSystemMessage(`${user.display_name} is now ${action.payload.user.display_name}`))

      user.user_name = action.payload.user.user_name
      user.display_name = action.payload.user.display_name

      return {
        ...state,
        users,
        messages
      }
    }

    case USER_EDITED_MESSAGE: {
      let messages = state.messages.slice()
      let message = state.messages.find((m) => {
        return m.message.id === action.payload.message.id
      })
      message.message.text = action.payload.message.text
      message.message.edited = true
      return {
        ...state,
        messages
      }
      return {
        ...state
      }
    }

    case USER_DELETED_MESSAGE: {
      let messages = state.messages.slice()
      let message = state.messages.find((m) => {
        return m.message.id === action.payload.message.id
      })
      message.message.text = '(deleted)'
      return {
        ...state,
        messages
      }
    }

    case USER_LEFT_CHAT: {
      let users = state.users.filter(user => user.id !== action.payload.user.id)
      let messages = state.messages.slice()
      messages.push(getSystemMessage(`${action.payload.user.display_name} has left the chat`))

      return {
        ...state,
        users,
        messages
      }
    }

    case END: {
      let messages = state.messages.slice()
      messages.push(getSystemMessage('END TRANSCRIPT'))
      return {
        ...state,
        messages
      }
    }

    case EVENT_PROCESS_ERROR: {
      console.log(EVENT_PROCESS_ERROR, action)
      return {
        ...state
      }
    }

    default: {
      return {
        ...state
      }
    }
  }

}
