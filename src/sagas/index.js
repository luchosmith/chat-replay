import { call, put, take, takeEvery, all, fork } from 'redux-saga/effects'
import { delay } from 'redux-saga'
import { fetchTranscript } from '../api'
import { MESSAGE,
         CONNECT,
         UPDATE,
         DELETE,
         DISCONNECT,
         UNKNOWN_PAYLOAD_TYPE,
         UNKNOWN_UPDATE_TYPE } from '../constants'
import { FETCH_TRANSCRIPT,
         FETCH_TRANSCRIPT_LOADING,
         FETCH_TRANSCRIPT_SUCCESS,
         FETCH_TRANSCRIPT_ERROR, 
         USER_POSTED_MESSAGE,
         USER_JOINED_CHAT,
         USER_EDITED_MESSAGE,
         USER_CHANGED_NAME,
         USER_LEFT_CHAT,
         USER_DELETED_MESSAGE,
         END,
         EVENT_PROCESS_ERROR } from '../actionTypes'


function* fetchTranscriptSaga({url}) {
  yield put({ type: FETCH_TRANSCRIPT_LOADING})
  const payload = yield call(fetchTranscript, url)
  if (payload.data) {
    yield put({ type: FETCH_TRANSCRIPT_SUCCESS, transcript: payload.data })
  } else {
    yield put({ type: FETCH_TRANSCRIPT_ERROR })
  }
}

function* processEvent(event) {
  yield call(delay, event.delta)
  switch(event.payload.type) {
    case MESSAGE: {
      yield put({ type: USER_POSTED_MESSAGE, payload:event.payload})
      break;
    }
    case CONNECT: {
      yield put({ type: USER_JOINED_CHAT, payload:event.payload })
      break;
    }
    case UPDATE: {
      if(event.payload.user) {
        yield put({ type: USER_CHANGED_NAME, payload:event.payload })
        break;
      }
      if(event.payload.message) {
        yield put({ type: USER_EDITED_MESSAGE, payload:event.payload})
        break;
      }
      yield put({ type: EVENT_PROCESS_ERROR, message: UNKNOWN_UPDATE_TYPE })
      break;
    }
    case DELETE: {
      yield put({ type: USER_DELETED_MESSAGE, payload:event.payload})
      break;
    }
    case DISCONNECT: {
      yield put({ type: USER_LEFT_CHAT, payload:event.payload})
      break;
    }
    case END: {
      yield put({ type: END })
    }
    default: {
      yield put({ type: EVENT_PROCESS_ERROR, message: UNKNOWN_PAYLOAD_TYPE })
      break;
    }
  }
}

function* transcriptSuccessSaga() {
    const {transcript} = yield take(FETCH_TRANSCRIPT_SUCCESS)
    let lastDelta = transcript[transcript.length-1].delta + 250
    transcript.push({delta:lastDelta, payload:{type:END}});
    for (let i=0; i<transcript.length; i++) {
      yield fork(processEvent, transcript[i])
    }
}

function* watchFetchTranscriptSaga(url) {
  yield takeEvery(FETCH_TRANSCRIPT, fetchTranscriptSaga)
}

export default function* saga() {
  yield all([
    call(watchFetchTranscriptSaga),
    call(transcriptSuccessSaga)
  ])
}
