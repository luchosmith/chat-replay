import { connect } from 'react-redux'
import Chat from '../components/Chat'
import { fetchTranscript } from '../actions'

const mapStateToProps = (state, ownProps) => {
  return {
    loading: state.loading,
    messages: state.messages,
    error: state.error
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onFetchTranscript: (url) => {
      dispatch(fetchTranscript(url))
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Chat)
