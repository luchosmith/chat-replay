import { FETCH_TRANSCRIPT } from '../actionTypes'

export const fetchTranscript = (url) => {
  return { type: FETCH_TRANSCRIPT, url}
}